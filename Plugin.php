<?php namespace Wpstudio\AssetsManifest;

use Backend;
use Cms\Classes\Theme;
use System\Classes\PluginBase;
use Wpstudio\AssetsManifest\Classes\Bundlers\Bundler;
use Wpstudio\AssetsManifest\Classes\Bundlers\ViteBundler;
use Wpstudio\AssetsManifest\Classes\ManifestReader;
use Wpstudio\AssetsManifest\Classes\TwigFilters;
use Config;

/**
 * assetsmanifest Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Assets manifest',
            'description' => 'Assets with manifest.json',
            'author'      => 'wpstudio',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        app()->singleton(Bundler::class, Config::get('wpstudio.assetsmanifest::bundler'));

        if (Config::get('wpstudio.assetsmanifest::vite_dev_enabled') != ViteBundler::class ||
            !Config::get('wpstudio.assetsmanifest::vite_dev_enabled')
        ) {
            app()->singleton(ManifestReader::class, fn() => new ManifestReader(
                Theme::getActiveTheme()->getPath(
                    sprintf(
                        '%s/%s/manifest.json',
                        Theme::getActiveTheme()->getDirName(),
                        Config::get('wpstudio.assetsmanifest::vite_path_build')
                    )
                ),
            ));
        }
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [

        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'wpstudio.assetsmanifest.some_permission' => [
                'tab' => 'assetsmanifest',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'assetsmanifest' => [
                'label'       => 'assetsmanifest',
                'url'         => Backend::url('wpstudio/assetsmanifest/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['wpstudio.assetsmanifest.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'encoreAsset' => [TwigFilters::class, 'encoreAsset'],
            ],
            'functions' => [
                'encoreAsset' => [TwigFilters::class, 'encoreAsset'],
                'hmrAssets' => [TwigFilters::class, 'hmrAssets'],
                'viteEntrypointStyles' => [TwigFilters::class, 'viteEntrypointStyles'],
                'viteEntrypointAssets' => [TwigFilters::class, 'viteEntrypointAssets'],
                'viteDevClientScriptTag' => [TwigFilters::class, 'viteDevClientScriptTag'],
            ],
        ];
    }
}
