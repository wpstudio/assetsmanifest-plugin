# Env file example

## Vitejs
```bash
VITE_DEV_SERVER_PROTOCOL=http
VITE_DEV_SERVER_HOST=localhost
VITE_DEV_SERVER_PORT=3000

VITE_PATH_SRC=assets/src
VITE_DIR_ENTRYPOINTS=entrypoints
VITE_PATH_BUILD=assets/build
VITE_ENTRYPOINTS_FILE=entrypoints.json
VITE_DEV_ENABLED=true
```
