<?php namespace Wpstudio\AssetsManifest\Classes;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

class ManifestReader implements Arrayable
{
    private Collection $manifest;

    /**
     * @param string $manifestPath
     * @throws AssetsManifestException
     */
    public function __construct(string $manifestPath)
    {
        if (!file_exists($manifestPath)) {
            throw new AssetsManifestException('Not found: ' . $manifestPath);
        }

        if (!is_readable($manifestPath)) {
            throw new AssetsManifestException('Not readable: ' . $manifestPath);
        }

        $this->manifest = collect(json_decode(file_get_contents($manifestPath), true));
    }

    public function get(string $assetName)
    {
        return $this->manifest->offsetGet($assetName);
    }

    public function toArray()
    {
        return $this->manifest;
    }
}
