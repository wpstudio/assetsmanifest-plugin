<?php namespace Wpstudio\AssetsManifest\Classes;

use Wpstudio\AssetsManifest\Classes\Bundlers\Bundler;
use Wpstudio\AssetsManifest\Classes\Bundlers\ViteBundler;
use Wpstudio\AssetsManifest\Classes\Bundlers\WebpackEncoreBundler;

class TwigFilters
{
    /**
     * @param string $assetName
     * @return string
     * @throws AssetsManifestException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function encoreAsset(string $assetName): string
    {
        $bundler = app()->get(Bundler::class);
        assert($bundler instanceof Bundler);
        $bundler->validateBundlerType(Bundler::BUNDLER_WEBPACK_ENCORE);
        assert($bundler instanceof WebpackEncoreBundler);

        return $bundler->getEntrypoint($assetName);
    }

    public static function hmrAssets()
    {
        if (\Config::get('app.debug')) {

            $bundler = app()->get(Bundler::class);
            assert($bundler instanceof Bundler);
            $bundler->validateBundlerType(Bundler::BUNDLER_WEBPACK_ENCORE);
            assert($bundler instanceof WebpackEncoreBundler);

            $manifestReader = app()->get(ManifestReader::class);
            assert($bundler instanceof ManifestReader);

            $startsWithSubstring = 'vendors-node_modules';

            foreach ($manifestReader->toArray() as $assetName => $assetsFullPath) {
                if (starts_with($assetName, $startsWithSubstring)) {
                    if (ends_with($assetName, '.css')) {
                        echo '<link rel="stylesheet" href="' . $assetsFullPath . '">' . PHP_EOL;
                    } else {
                        echo '<script src="' . $assetsFullPath . '"></script>' . PHP_EOL;
                    }
                }
            }
        }
    }

    /**
     * @param string $entrypointName
     * @return string
     * @throws AssetsManifestException
     */
    public static function viteEntrypointStyles(string $entrypointName): string
    {
        $bundler = app()->get(Bundler::class);
        assert($bundler instanceof Bundler);
        $bundler->validateBundlerType(Bundler::BUNDLER_VITE);
        assert($bundler instanceof ViteBundler);

        if ($bundler->isViteDevEnabled()) {
            return '';
        }

        return $bundler->getEntrypointStylesheets($entrypointName);
    }

    /**
     * @param string $entrypointName
     * @return string
     * @throws AssetsManifestException
     */
    public static function viteEntrypointAssets(string $entrypointName): string
    {
        $bundler = app()->get(Bundler::class);
        assert($bundler instanceof Bundler);
        $bundler->validateBundlerType(Bundler::BUNDLER_VITE);
        assert($bundler instanceof ViteBundler);

        if ($bundler->isViteDevEnabled()) {
            return $bundler->getScriptTag($entrypointName);
        } else {
            return $bundler->getEntrypointAssets($entrypointName);
        }
    }

    public static function viteDevClientScriptTag()
    {
        $bundler = app()->get(Bundler::class);
        assert($bundler instanceof Bundler);
        $bundler->validateBundlerType(Bundler::BUNDLER_VITE);
        assert($bundler instanceof ViteBundler);

        if ($bundler->isViteDevEnabled()) {
            return $bundler->getViteDevClientScriptTag();
        }

        return '';
    }
}
