<?php namespace Wpstudio\AssetsManifest\Classes\Bundlers;

use Wpstudio\AssetsManifest\Classes\AssetsManifestException;
use Wpstudio\AssetsManifest\Classes\ManifestReader;

abstract class Bundler
{
    const BUNDLER_WEBPACK_ENCORE = 'webpack-encore';
    const BUNDLER_VITE = 'vite';

    public static array $bundlers = [
        self::BUNDLER_WEBPACK_ENCORE => WebpackEncoreBundler::class,
        self::BUNDLER_VITE => ViteBundler::class,
    ];

    protected ?ManifestReader $manifestReader;

    public function __construct(?ManifestReader $manifestReader = null)
    {
        if (isset($manifestReader)) {
            $this->manifestReader = $manifestReader;
        }
    }

    public function getBundlerType(): string
    {
        return array_flip(self::$bundlers)[get_class($this)];
    }

    public function validateBundlerType(string $bundlerType)
    {
        if ($this->getBundlerType() != $bundlerType) {
            throw new AssetsManifestException(
                sprintf(
                    'Expected bundler type is %s, but actual %s',
                    $bundlerType,
                    $this->getBundlerType()
                )
            );
        }
    }

    public function getEntrypoint(string $entrypointPathInManifest): array
    {
        return $this->manifestReader->get($entrypointPathInManifest);
    }
}
