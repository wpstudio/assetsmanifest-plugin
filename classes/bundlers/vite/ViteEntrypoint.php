<?php namespace Wpstudio\AssetsManifest\Classes\Bundlers\Vite;

use Illuminate\Support\Collection;

class ViteEntrypoint
{
    public string $file;
    public ?string $src;
    public ?bool $isEntry;
    public ?Collection $imports;
    public ?Collection $css;

    public function __construct(array $entrypoint)
    {
        $this->file = $entrypoint['file'];

        if (array_key_exists('src', $entrypoint)) {
            $this->isEntry = $entrypoint['src'];
        }

        if (array_key_exists('isEntry', $entrypoint)) {
            $this->isEntry = $entrypoint['isEntry'];
        }

        if (array_key_exists('imports', $entrypoint)) {
            $this->imports = collect($entrypoint['imports']);
        }

        if (array_key_exists('css', $entrypoint)) {
            $this->css = collect($entrypoint['css']);
        }
    }

    public function getIsEntry(): bool
    {
        if (isset($this->isEntry) && $this->isEntry) {
            return true;
        }

        return false;
    }

    public function hasImports()
    {
        return isset($this->imports) && $this->imports;
    }

    public function getImports(): Collection
    {
        return $this->imports;
    }

    public function hasCss()
    {
        return isset($this->css) && $this->css;
    }

    public function getCss(): Collection
    {
        return $this->css;
    }

    public function getFile(): string
    {
        return $this->file;
    }

    public function getSrc(): string
    {
        return $this->src;
    }
}
