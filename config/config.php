<?php return [
    'vite_dev_enabled' => env('VITE_DEV_ENABLED', true),
    'vite_dev_server_protocol' => env('VITE_DEV_SERVER_PROTOCOL', 'http'),
    'vite_dev_server_host' => env('VITE_DEV_SERVER_HOST', 'localhost'),
    'vite_dev_server_port' => env('VITE_DEV_SERVER_PORT', '3000'),
    'vite_path_src' => env('VITE_PATH_SRC', 'assets/src'),
    'vite_dir_entrypoints' => env('VITE_DIR_ENTRYPOINTS', 'entrypoints'),
    'vite_path_build' => env('VITE_PATH_BUILD', 'assets/build'),
    'bundler' => \Wpstudio\AssetsManifest\Classes\Bundlers\ViteBundler::class
];
