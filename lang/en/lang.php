<?php

return [
    'components' => [
        'manifest' => [
            'name' => 'Manifest path',
            'description' => 'Accept manifest.json path',
            'properties' => [
                'path' => [
                    'title' => 'Manifest relative basepath',
                    'description' =>
                        'manifest.json path relative from basepath. Example: themes/demo/assets/build/manifest.json',
                    'validationMessage' => 'Incorrect manifest.json path',
                ]
            ]
        ]
    ]
];
