<?php

return [
    'components' => [
        'manifest' => [
            'name' => 'Manifest path',
            'description' => 'Устанавливает путь к manifest.json',
            'properties' => [
                'path' => [
                    'title' => 'Manifest относительно корня',
                    'description' =>
            'Путь к файлу manifest.json относительно корня проекта. Например: themes/demo/assets/build/manifest.json',
                    'validationMessage' => 'Неправильный формат пути к файлу manifest.json',
                ]
            ]
        ]
    ]
];
